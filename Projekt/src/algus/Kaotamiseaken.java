package algus;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Klass sisaldab meetodid, mis kuvab kaotamise akna
 *
 * @author Marten Tammeleht
 * @version 1.0
 */
public class Kaotamiseaken {
	/**
	 * Klassi muutuja
	 */
	static boolean vastus;
	
	/**
	 * Kuvab akna, kus kysitakse kasutajalt kas tahab uuesti m2ngida.
	 * @return true / false vastavalt, kas kasutaja tahab j2tkata v6i l6petada
	 */
	public static boolean ekraan (){
		Stage lava = new Stage();
		lava.setMinWidth(200);
		lava.setMinHeight(200);
		lava.initModality(Modality.APPLICATION_MODAL);
		
		Button eireset = new Button("Ei");
		Button reset = new Button("Jah");
		
		reset.setOnAction(e -> { 
			vastus = true;
			lava.close();
			});
		eireset.setOnAction(e -> {
			vastus = false;
			lava.close();
		});
		lava.setOnCloseRequest(e -> {
			vastus = false;
			lava.close();
		});
		Label label = new Label("Astusite miini otsa");
		Label label2 = new Label("Kas soovite veel proovida?");
		
		HBox nup = new HBox();
		nup.getChildren().addAll(reset,eireset);
		nup.setAlignment(Pos.CENTER);
		nup.setSpacing(5);
		
		VBox layout = new VBox();
		layout.setAlignment(Pos.CENTER);
		layout.getChildren().addAll(label,label2,nup);
		Scene scene = new Scene(layout,200,200);
		lava.setScene(scene);
		lava.showAndWait();
		
		return vastus;
		
	}//ekraan

}//Kaotamiseaken