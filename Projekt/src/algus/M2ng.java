package algus;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Klass, milles tehakse valmis m2ngu ekraan ja antakse talle vajalikud andmed
 *
 * @author Marten Tammeleht
 * @version 1.0
 */
public class M2ng {
	/**
	 * Klassi muutuja, mis hoiab v22rtust palju on m2ngu v2ljal pomme m2rgitud
	 */
	static int pommeM2rgitud = 0;
	/**
	 * Klassi muutuja, mis hoiab v22rtust palju on lahtreid avatud m2ngus
	 */
	static int lahtreidAvatud = 0;
	/**
	 * Meetod, mis loob p6him2ngu akna
	 * 
	 */
	public static void alustaM2ngu(){
		Stage stage = new Stage();
		
		int miiniv2ljalaius = Muutmiseaken.miinLaius;
		int miiniv2ljak6rgus = Muutmiseaken.miinK6rgus;
		int miiniv2ljasuurus = miiniv2ljalaius * miiniv2ljak6rgus;
		int miinidearv = (int)(miiniv2ljasuurus*0.15);
		
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(5,5,5,5));
		grid.setVgap(1);
		grid.setHgap(1);
		
		HBox alumineosa = new HBox();
		HBox ylemineosa = new HBox();
		ylemineosa.setSpacing(5);
		ylemineosa.setAlignment(Pos.CENTER);
		ylemineosa.setPadding(new Insets(5,5,5,5));
		alumineosa.setAlignment(Pos.CENTER);
		alumineosa.setPadding(new Insets(0,10,10,0));
		alumineosa.setSpacing(10);
		
		String miinidem2rk = Integer.toString(miinidearv-pommeM2rgitud);
		Label label2 = new Label("Miinide arv:");
		Label label = new Label(miinidem2rk);
		label.setFont(Font.font("Arial", 18));
		Button[][] nupud = new Button[miiniv2ljak6rgus][miiniv2ljalaius];
		
		int[] numbridi = new int[miiniv2ljak6rgus];
		int[] numbridj = new int[miiniv2ljalaius];
		int[][] consnumbrid = new int[miiniv2ljak6rgus][miiniv2ljalaius];
		
		for(int i = 0;i<nupud.length;i++){
			for(int j = 0;j<nupud[i].length;j++){
				numbridj[j] = j;
				consnumbrid[i][j]=0;
			}
			numbridi[i] = i;
		}
		
		int[][] miiniv2li = Miiniv2li.teemeMiiniV2lja(miiniv2ljak6rgus, miiniv2ljalaius,miinidearv);
		
		Button reset = new Button("Reset");
		reset.setOnAction(e -> { 
			stage.close();
			pommeM2rgitud = 0;
			lahtreidAvatud = 0;
			M2ng.alustaM2ngu();
		});
		
		Button muudasuurust = new Button("Muuda miiniv2lja");
		muudasuurust.setOnAction(e -> {
			if(Muutmiseaken.ekraan()){ 
				stage.close();
				pommeM2rgitud = 0;
				lahtreidAvatud = 0;
				M2ng.alustaM2ngu();
			}
		});
		
		for(int i = 0; i<nupud.length;i++){
			for(int j = 0;j<nupud[i].length;j++){
				int g = numbridi[i];
				int h = numbridj[j];
				
				nupud[i][j] = new Button();
				nupud[i][j].setMinSize(25, 25);
				nupud[i][j].setMaxSize(25, 25);
				grid.add(nupud[i][j], j, i);
				nupud[i][j].setGraphic(new ImageView(Pildid.nuputaust));
				
				nupud[i][j].setOnMouseClicked(e -> {
					if(e.getButton() == MouseButton.SECONDARY){ 
						if(consnumbrid[g][h] == 0){ 
							pommeM2rgitud++;
							consnumbrid[g][h] = 1;
							nupud[g][h].setGraphic(new ImageView(Pildid.lipp));
						}else{ 
							pommeM2rgitud--;
							consnumbrid[g][h] = 0;
							nupud[g][h].setGraphic(new ImageView(Pildid.nuputaust));
						}
						String miine = Integer.toString(miinidearv-pommeM2rgitud);
						Label label1 = new Label(miine);
						label1.setFont(Font.font("Arial", 18));
						ylemineosa.getChildren().removeAll();
						ylemineosa.getChildren().setAll(label2,label1);
					}
					
				});//onmouseclick
				
				nupud[i][j].addEventFilter(ActionEvent.ACTION, e -> {
					if(consnumbrid[g][h] == 1) {
						e.consume();
					}
				});//eventfilter
				
				nupud[i][j].setOnAction(e -> {
				    if(miiniv2li[g][h] == 0) {  
				    	nupud[g][h].setGraphic(new ImageView(Pildid.tyhi));
						nupud[g][h].setDisable(true);
						if( -1 != h-1) {
							nupud[g][h-1].fire();
						}
						if( -1 != g-1) {
							nupud[g-1][h].fire();
						}
						if( miiniv2ljalaius != h+1) {
							nupud[g][h+1].fire();
						}
						if( miiniv2ljak6rgus != g+1) {
							nupud[g+1][h].fire();
						}
						if( -1 != (h-1) && -1 != (g-1)) {
							nupud[g-1][h-1].fire();
						}
						if( miiniv2ljalaius != (h+1) && -1 != (g-1)) {
							nupud[g-1][h+1].fire();
						}
						if( -1 != (h-1) && miiniv2ljak6rgus != (g+1)) {
							nupud[g+1][h-1].fire();
						}
						if( miiniv2ljalaius != (h+1) && miiniv2ljak6rgus != (g+1)) {
							nupud[g+1][h+1].fire();
						}
					}
					if(miiniv2li[g][h] == 1) {  
						nupud[g][h].setGraphic(new ImageView(Pildid.yks));
					} 
					if(miiniv2li[g][h] == 2) { 
						nupud[g][h].setGraphic(new ImageView(Pildid.kaks));
					} 
					if(miiniv2li[g][h] == 3) {  
						nupud[g][h].setGraphic(new ImageView(Pildid.kolm));
					}
					if(miiniv2li[g][h] == 4) {  
						nupud[g][h].setGraphic(new ImageView(Pildid.neli));
					} 
					if(miiniv2li[g][h] == 5) {  
						nupud[g][h].setGraphic(new ImageView(Pildid.viis));
					} 
					if(miiniv2li[g][h] == 6) {  
						nupud[g][h].setGraphic(new ImageView(Pildid.kuus));
					} 
					if(miiniv2li[g][h] == 7) {  
						nupud[g][h].setGraphic(new ImageView(Pildid.seitse));
					} 
					if(miiniv2li[g][h] == 8) {  
						nupud[g][h].setGraphic(new ImageView(Pildid.kaheksa));
					} 
					if(miiniv2li[g][h] == 9) {  
						nupud[g][h].setGraphic(new ImageView(Pildid.pomm));
						boolean kasalustabuuesti = Kaotamiseaken.ekraan();
						if(kasalustabuuesti){
							reset.fire();
						}else{
							Platform.exit();
						}
					} 
					nupud[g][h].setDisable(true); 
					nupud[g][h].setOpacity(1);
					lahtreidAvatud++;
					if(lahtreidAvatud == miiniv2ljasuurus-miinidearv){
						boolean kasveelkord = V6itmiseaken.ekraan();
						if(kasveelkord){ 
							reset.fire();
						}else{ 
							Platform.exit();
						}
					}
				});//setonaction
				
				
			}
		}//for tsykli l6pp
		
		int stseenilaius = (miiniv2ljalaius*25 + 10 + miiniv2ljalaius * 1 - 1 );
		int stseenik6rgus = (miiniv2ljak6rgus*25 + 10 + miiniv2ljak6rgus * 1 - 1 + 70);
				
		alumineosa.getChildren().addAll(reset,muudasuurust);
		ylemineosa.getChildren().addAll(label2,label);
		grid.setAlignment(Pos.CENTER);
		
		BorderPane pane = new BorderPane();
		pane.setCenter(grid);
		pane.setBottom(alumineosa);
		pane.setTop(ylemineosa);
		
		Scene stseen = new Scene(pane, stseenilaius,stseenik6rgus);
		stage.setTitle("MineSweeper");
		stage.setScene(stseen);
		stage.setResizable(false);
		stage.show();

	}//alustaM2ngu
	
}//M2ng
