package algus;


import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Klass mis hoiab miiniv2lja suurusi ja sisaldab meetodit millega neid muuta
 *
 * @author Marten Tammeleht
 * @version 1.0
 */
public class Muutmiseaken {
	
	/**
	 * Klassi muutuja, hoiab miiniv2lja laiuse v22rtust
	 */
	public static int miinLaius = 10;
	/**
	 * Klassi muutuja, hoiab miiniv2lja k6rguse v22rtust
	 */
	public static int miinK6rgus = 10;
	/**
	 * Klassi muutuja
	 */
	public static boolean vastus;
	/**
	 * Meetod, mis kuvab akna, kus kysitakse kasutajalt, kas tahab muuta miiniv2lja suurust ja kui tahab, mis on uued suurused
	 * @return true / false vastavalt, kas kasutaja tahab muuta miiniv2lja suurust v6i mitte
	 */
	public static boolean ekraan (){
		Stage lava = new Stage();
		lava.initModality(Modality.APPLICATION_MODAL);
		
		Button tagasi = new Button("Tagasi");
		Button muuda = new Button("Muuda");
		
		TextField andmedlaius = new TextField();
		TextField andmedk6rgus = new TextField();
		andmedlaius.setPromptText("Uus laius");
		andmedk6rgus.setPromptText("Uus K6rgus");
		andmedk6rgus.setAlignment(Pos.CENTER);
		andmedlaius.setAlignment(Pos.CENTER);
		andmedk6rgus.setMaxWidth(150);
		andmedlaius.setMaxWidth(150);
		
		Label k6rgus = new Label("K6rgus hetkel:"+miinK6rgus);
		Label laius = new Label("Laius hetkel:"+miinLaius);
		Label error = new Label("Sisesta number");
		Label error2 = new Label("Arv peab olema vahemikus \n 10 kuni 50");
		error.setTextFill(Color.RED);
		error2.setTextFill(Color.RED);
		VBox layout = new VBox();
		
		layout.setAlignment(Pos.CENTER);
		layout.getChildren().addAll(laius,andmedlaius,k6rgus,andmedk6rgus,muuda,tagasi);
				
		muuda.setOnAction(e -> {
			layout.getChildren().remove(error2);
			layout.getChildren().remove(error);
			if(onNumber(andmedlaius) && onNumber(andmedk6rgus)){ 
				int k6rguss = Integer.parseInt(andmedk6rgus.getText());
				int laiuss = Integer.parseInt(andmedlaius.getText());
				if(k6rguss >= 10 && laiuss >= 10 && k6rguss <= 50 && laiuss <= 50){
					lava.close();
					miinLaius = laiuss;
					miinK6rgus = k6rguss;
					vastus = true;					
				}else{ 
					layout.getChildren().add(error2);
				}
				
			}else{ 
				layout.getChildren().add(error);
			}
		});
		tagasi.setOnAction(e -> {
			lava.close();
			vastus = false;
		});
		
		Scene scene = new Scene(layout,200,200);
		lava.setScene(scene);
		lava.showAndWait();
		return vastus;
		
	}//ekraan
	
	/**
	 * Meetod, mis vaatab kas antud sisestus teksti lahtrisse on number
	 * @param andmed teksti lahter
	 * @return true / false vastavalt kas on number v6i ei
	 */
	private static boolean onNumber(TextField andmed){
		try{
			Integer.parseInt(andmed.getText());
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}//onNumber
}

