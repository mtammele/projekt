package algus;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Klass mis sisaldab m2ngu alustavat meetodit. Laiendab klassi Application
 *
 * @author Marten Tammeleht
 * @version 1.0
 */
public class M2ngualustamine extends Application{
	/**
	 * P6himeetod, mis alustab protsessi
	 * @param args ei kasutata
	 */
	public static void main(String[] args) {
		launch();
	}//main
	/**
	 * Kutsub v2lja m2ngu
	 * @param stage ei kasutata 
	 */
	@Override
	public void start(Stage stage) throws Exception {
		M2ng.alustaM2ngu();			
	}//start
	
	
}//M2ngualustamine
