package algus;

/**
 * Klass hoiab meetodeid masiivi tegemiseks kuhu pannakse miinid ja m2rgitakse nende asukoht
 *
 * @author Marten Tammeleht
 * @version 1.0
 */
public class Miiniv2li {

	/**
	 * Loob miini v2lja antud parameetritega
	 * @param miiniv2ljalaius Miiniv2lja laius
	 * @param miiniv2ljak6rgus Miiniv2lja k6rgus
	 * @param miinidearv Miinide arv
	 * @return Miiniv2lja massiiv, mis sisaldab miine ehk 9 ja numbreid mitu on miine vastava asukoha ymber
	 */
	public static int[][] teemeMiiniV2lja(int miiniv2ljalaius, int miiniv2ljak6rgus, int miinidearv){
		int[][] grid = new int[miiniv2ljalaius][miiniv2ljak6rgus];
		int[][] muutuv = new int[miiniv2ljalaius][miiniv2ljak6rgus];
		for(int i = 0; i < grid.length ; i++){
			for(int j = 0; j < grid[i].length ; j++){
				grid[i][j] = 0;
			}
		}
		int pikkus = grid.length;
		int k6rgus = grid[0].length;
		
		for(int i = 0; i < miinidearv ; i++){
			while(true){
				int r = suvalineArv(pikkus);
				int t = suvalineArv(k6rgus);
				if( grid[r][t] != 9){
					grid[r][t] = 9;
					break;
				}
			}
		}
		for(int i = 0; i < grid.length; i++){
			for(int j = 0 ; j < grid[i].length; j++){
				muutuv[i][j] = grid[i][j];
			}
		}
		int[][] l6pp = muutus(muutuv);
		return l6pp;
	}//teemeMiiniV2lja
	
	/**
	 * N�itab mitu miini on antud v2lja juures
	 * @param muut Masiiv kus on olemas miinid ehk 9
	 * @return Masiiv millel on m2rgitud mitu pommi on v2lja juures
	 */
	public static int[][] muutus(int[][] muut ){
		for(int i = 0; muut.length > i ; i++){
			for(int j = 0; j < muut[i].length ; j++){
				int h = 0;
				if( i-1 >= 0 && j+1 < muut[i].length){
					if(muut[i-1][j+1] == 9 ){
						h++;
					}
				}
				if( i >= 0 && j+1 < muut[i].length){
					if(muut[i][j+1] == 9 ){
						h++;
					}
				}
				if( i+1 < muut.length && j+1 < muut[i].length){
					if(muut[i+1][j+1] == 9 ){
						h++;
					}
				}
				if( i-1 >= 0){
					if(muut[i-1][j] == 9 ){
						h++;
					}
				}
				if( i+1 < muut.length ){
					if(muut[i+1][j] == 9 ){
						h++;
					}
				}
				if( i-1 >= 0 && j-1 >= 0){
					if(muut[i-1][j-1] == 9 ){
						h++;
					}
				}
				if( j-1 >= 0){
					if(muut[i][j-1] == 9 ){
						h++;
					}
				}
				if( i+1 < muut.length && j-1 >= 0){
					if(muut[i+1][j-1] == 9 ){
						h++;
					}
				}
				if( muut[i][j] == 0){
					if( h == 0 ){
						muut[i][j] = 0;
					}else if( h == 1 ){
						muut[i][j] = 1;
					}else if( h == 2){
						muut[i][j] = 2;
					}else if( h == 3){
						muut[i][j] = 3;
					}else if(h == 4){
						muut[i][j] = 4;
					}else if( h == 5){
						muut[i][j] = 5;
					}else if( h == 6){
						muut[i][j] = 6;
					}else if( h == 7){
						muut[i][j] = 7;
					}else if( h == 8){
						muut[i][j] = 8;
					}
				}
			}
			
		}
		
		return muut;
	}//muutus
	/**
	 * Arvutab suvalise arvu
	 * @param a kui suure arvu hulgast tahetakse suvalist arvu
	 * @return Suvaline arv 0-a
	 */
	public static int suvalineArv( int a){
		int suvalinearv = (int)(Math.random() * a);
		return suvalinearv;
	}//suvalineArv


}//Miiniv2li
